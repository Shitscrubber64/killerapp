﻿using RPGLibrary.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace RPGLibrary.Repositories
{
    /// <summary>
    /// Repository that can fetch, delete and add Items.
    /// </summary>
    public interface IItemRepo
    {
        List<Item> DumpItems();
        void DeleteItem(int id);
        void AddItem(Item newitem);
        void FixURL();
    }
}
