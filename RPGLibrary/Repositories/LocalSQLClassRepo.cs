﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data.SqlClient;
using RPGLibrary.Entities;
using System.Data;

namespace RPGLibrary.Repositories
{
    /// <summary>
    /// Repository that can fetch, add and delete Classes via SQL.
    /// </summary>
    public class LocalSQLClassRepo : IClassRepo
    {
        public SqlConnection conn;
        public LocalSQLClassRepo()
        {
            conn = new SqlConnection("Data source = localhost; Initial Catalog = DBO_FUN; Integrated Security = True");
            try
            {
                conn.Open(); // this connection is currently never closed in the application (stupid, I know)
            }
            catch (Exception)
            {
                throw;
            }
        }

        public List<Class> DumpClasses()
        {
            SqlCommand command = new SqlCommand("SELECT * FROM table_classes", conn);

            List<Class> data = new List<Class>();

            using (SqlDataReader reader = command.ExecuteReader())
            {
                while (reader.Read())
                {
                    Class currentclass = new Class(Convert.ToInt16(reader["id_class"]), reader["name"] + "", reader["description"] + "", reader["spriteurl"] + "", Convert.ToDecimal(reader["base_atk"]), Convert.ToDecimal(reader["base_def"]), Convert.ToDecimal(reader["base_spatk"]), Convert.ToDecimal(reader["base_spdef"]), Convert.ToDecimal(reader["base_spd"]));
                    data.Add(currentclass);
                }
            }
            return data; // might consider returning as a readonly list later on
        }

        public List<Class> FillWithMoves(List<Class> classes) // sets moveset for each class, not automatically done
        {
            foreach (Class current in classes)
            {
                SqlCommand command = new SqlCommand("SELECT table_moves.name FROM table_moves INNER JOIN table_classes_moves ON table_moves.id_move = table_classes_moves.id_move WHERE table_classes_moves.id_class = @id", conn);
                command.Parameters.AddWithValue("@id", current.id);

                List<String> data = new List<String>();

                using (SqlDataReader reader = command.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        data.Add(reader["name"] + "");
                    }
                }
                current.moveset = data;
            }
            return classes;
        }

        public void DeleteClass(int id)
        {
            // first remove any relevant moves and heroes before foreign keys start whining again
            SqlCommand command = new SqlCommand("DELETE FROM table_classes_moves WHERE id_class = @id", conn);
            command.Parameters.AddWithValue("@id", id);
            command.ExecuteNonQuery();
            command = new SqlCommand("DELETE FROM table_heroes WHERE id_class = @id", conn);
            command.Parameters.AddWithValue("@id", id);
            command.ExecuteNonQuery();

            // now remove the actual class
            command = new SqlCommand("DELETE FROM table_classes WHERE id_class = @id", conn);
            command.Parameters.AddWithValue("@id", id);
            command.ExecuteNonQuery();
        }

        public void AddClass(Class newclass)
        {
            SqlCommand command = new SqlCommand("INSERT INTO table_classes(name, description, base_atk, base_def, base_spatk, base_spdef, base_spd) VALUES(@name, @desc, @atk, @def, @spatk, @spdef, @spd)", conn);
            command.Parameters.AddWithValue("@name", newclass.name);
            command.Parameters.AddWithValue("@desc", newclass.desc);
            command.Parameters.AddWithValue("@atk", newclass.base_atk / 10); // values come in 10x higher than they should to make the HTML sliders easier to use, so now we need to divide them by 10
            command.Parameters.AddWithValue("@def", newclass.base_def / 10);
            command.Parameters.AddWithValue("@spatk", newclass.base_spatk / 10);
            command.Parameters.AddWithValue("@spdef", newclass.base_spdef / 10);
            command.Parameters.AddWithValue("@spd", newclass.base_spd / 10);
            command.ExecuteNonQuery();
        }

        public void AddMove(int classid)
        {
            SqlCommand command = new SqlCommand("addMoveToClassAutomatic", conn);
            command.CommandType = CommandType.StoredProcedure;
            command.Parameters.Add(new SqlParameter("@idclass", classid));
            command.ExecuteNonQuery();
        }
    }
}
