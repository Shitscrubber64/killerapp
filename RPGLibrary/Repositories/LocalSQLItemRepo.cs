﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data.SqlClient;
using RPGLibrary.Entities;
using System.Data;

namespace RPGLibrary.Repositories
{
    /// <summary>
    /// Repository that can fetch, add and delete Items via SQL.
    /// </summary>
    public class LocalSQLItemRepo : IItemRepo
    {
        public SqlConnection conn;
        public LocalSQLItemRepo()
        {
            conn = new SqlConnection("Data Source = localhost; Initial Catalog = DBO_FUN; Integrated Security = True");
            try
            {
                conn.Open(); // this connection is currently never closed in the application (stupid, I know)
            }
            catch (Exception)
            {
                throw;
            }
        }

        public List<Item> DumpItems()
        {
            SqlCommand command = new SqlCommand("SELECT * FROM table_items", conn);

            List<Item> data = new List<Item>();

            using (SqlDataReader reader = command.ExecuteReader())
            {
                while (reader.Read())
                {
                    Item current = new Item(Convert.ToInt16(reader["id_item"]), reader["name"] + "", reader["description"] + "", reader["spriteurl"] + "", Convert.ToDecimal(reader["mod_atk"]), Convert.ToDecimal(reader["mod_def"]), Convert.ToDecimal(reader["mod_spatk"]), Convert.ToDecimal(reader["mod_spdef"]), Convert.ToDecimal(reader["mod_spd"]));
                    current.SetSlotByInt(Convert.ToInt16(reader["slot"]));
                    data.Add(current);
                }
            }
            return data; // might consider returning as a readonly list later on
        }

        public void DeleteItem(int id)
        {
            // remove the actual item, no foreign keys to worry about in other tables
            SqlCommand command = new SqlCommand("DELETE FROM table_items WHERE id_item = @id", conn);
            command.Parameters.AddWithValue("@id", id);
            command.ExecuteNonQuery();
        }

        public void AddItem(Item newitem)
        {
            SqlCommand command = new SqlCommand("INSERT INTO table_items(name, description, slot, mod_atk, mod_def, mod_spatk, mod_spdef, mod_spd) VALUES(@name, @desc, @slot, @atk, @def, @spatk, @spdef, @spd)", conn);
            command.Parameters.AddWithValue("@name", newitem.name);
            command.Parameters.AddWithValue("@desc", newitem.desc);

            // database expects an integer for slot
            int slotid;
            switch (newitem.slot)
            {
                case Slot.head:
                    slotid = 1;
                    break;
                case Slot.chest:
                    slotid = 2;
                    break;
                case Slot.hands:
                    slotid = 3;
                    break;
                case Slot.feet:
                    slotid = 4;
                    break;
                default: // weapon slot is 0
                    slotid = 0;
                    break;
            }
            command.Parameters.AddWithValue("@slot", slotid);

            command.Parameters.AddWithValue("@atk", newitem.mod_atk);
            command.Parameters.AddWithValue("@def", newitem.mod_def);
            command.Parameters.AddWithValue("@spatk", newitem.mod_spatk);
            command.Parameters.AddWithValue("@spdef", newitem.mod_spdef);
            command.Parameters.AddWithValue("@spd", newitem.mod_spd);
            command.ExecuteNonQuery();
        }

        public void FixURL()
        {
            SqlCommand command = new SqlCommand("setDefaultSpriteUrl", conn);
            command.CommandType = CommandType.StoredProcedure;
            command.ExecuteNonQuery();
        }
    }
}
