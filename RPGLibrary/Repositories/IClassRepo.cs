﻿using RPGLibrary.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace RPGLibrary.Repositories
{
    /// <summary>
    /// Repository that can fetch, delete and add Classes.
    /// </summary>
    public interface IClassRepo
    {
        List<Class> DumpClasses();
        List<Class> FillWithMoves(List<Class> classes);
        void DeleteClass(int id);
        void AddClass(Class newclass);
        void AddMove(int classid);
    }
}
