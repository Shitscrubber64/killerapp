﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RPGLibrary.Entities
{
    enum Target
    {
        Enemy,
        EnemyTeam,
        Self,
        SelfTeam
    }

    enum Type
    {
        Physical,
        Special,
        Other
    }

    public class Move
    {
        string name;
        string desc;
        int damage;
        Target target;
        Type type;
        int minlevel;
    }
}
