﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPGLibrary.Entities
{
    /// <summary>
    /// Enum determining whether this equippable items goes on your head, chest, hands, feet or in your hand (as weapon).
    /// </summary>
    public enum Slot
    {
        weapon = 0,
        head = 1,
        chest = 2,
        hands = 3,
        feet = 4
    }

    /// <summary>
    /// Entity that contains name, description, stat multipliers, etc...
    /// </summary>
    public class Item
    {
        public int id;
        public string name;
        public string desc;
        public string spriteurl;
        public Slot slot;
        public decimal mod_atk = 1;
        public decimal mod_def = 1;
        public decimal mod_spatk = 1;
        public decimal mod_spdef = 1;
        public decimal mod_spd = 1;

        public Item(int id, string name, string desc, string spriteurl, decimal atk, decimal def, decimal spatk, decimal spdef, decimal spd)
        {
            this.id = id;
            this.name = name;
            this.desc = desc;
            this.spriteurl = spriteurl;
            this.mod_atk = atk;
            this.mod_def = def;
            this.mod_spatk = spatk;
            this.mod_spdef = spdef;
            this.mod_spd = spd;

            // the slot enum is a little different (because Aurelia is too shit to automatically convert such data in any shape or form)
            // it's not taken care of in the constructor (so remember to set this as well everytime you instance a new item)
        }

        /// <summary>
        /// Sets this Item's slot ENUM to the given index. Useful for retrieving database values (which stores this ENUM as an INT).
        /// </summary>
        /// <param name="slot">Index to use from ENUM.</param>
        public void SetSlotByInt(int slot)
        {
            switch (slot)
            {
                case 1:
                    this.slot = Slot.head;
                    break;

                case 2:
                    this.slot = Slot.chest;
                    break;

                case 3:
                    this.slot = Slot.hands;
                    break;

                case 4:
                    this.slot = Slot.feet;
                    break;

                default: // slot 0 is weapon
                    this.slot = Slot.weapon;
                    break;
            }
        }

        /// <summary>
        /// Sets this Item's slot ENUM to whichever one equals the given string. Try avoiding this method; JSON automatically turns ENUM into INT, not STRING.
        /// </summary>
        /// <param name="slot">String value to look for in the ENUM.</param>
        public void SetSlotByString(string slot)
        {
            switch (slot)
            {
                case "head":
                    this.slot = Slot.head;
                    break;

                case "chest":
                    this.slot = Slot.chest;
                    break;

                case "hands":
                    this.slot = Slot.hands;
                    break;

                case "feet":
                    this.slot = Slot.feet;
                    break;

                default: // slot 0 is weapon
                    this.slot = Slot.weapon;
                    break;
            }
        }
    }
}
