﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RPGLibrary.Entities
{
    /// <summary>
    /// Entity that contains name, description, stats, etc...
    /// </summary>
    public class Class
    {
        public int id;
        public string name;
        public string desc;
        public string spriteurl; //spriteurl is only a portion of the url, like this: "img/sprites/[SPRITEURL].png"

        public decimal base_atk = 1.0m; // took a while to figure out, ending a value with "m" sets it as a decimal
        public decimal base_def = 1.0m;
        public decimal base_spatk = 1.0m;
        public decimal base_spdef = 1.0m;
        public decimal base_spd = 1.0m;

        public List<String> moveset = new List<String>();

        public Class(int id, string name, string desc, string spriteurl, decimal base_atk, decimal base_def, decimal base_spatk, decimal base_spdef, decimal base_spd)
        {
            this.id = id;
            this.name = name;
            this.desc = desc;
            this.spriteurl = spriteurl;
            this.base_atk = base_atk;
            this.base_def = base_def;
            this.base_spatk = base_spatk;
            this.base_spdef = base_spdef;
            this.base_spd = base_spd;
        }
    }
}
