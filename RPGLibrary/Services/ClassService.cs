﻿using RPGLibrary.Entities;
using RPGLibrary.Repositories;
using System;
using System.Collections.Generic;
using System.Text;

namespace RPGLibrary.Services
{
    public class ClassService
    {
        public IClassRepo classrepo;

        public ClassService(IClassRepo classrepo)
        {
            this.classrepo = classrepo;
        }

        public List<Class> DumpClasses()
        {
            return classrepo.DumpClasses();
        }

        public List<Class> FillWithMoves(List<Class> classes)
        {
            return classrepo.FillWithMoves(classes);
        }

        public void DeleteClass(int id)
        {
            classrepo.DeleteClass(id);
        }

        public void AddClass(Class newclass)
        {
            classrepo.AddClass(newclass);
        }

        public void AddMove(int classid)
        {
            classrepo.AddMove(classid);
        }
    }
}
