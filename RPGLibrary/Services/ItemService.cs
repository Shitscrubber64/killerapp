﻿using RPGLibrary.Entities;
using RPGLibrary.Repositories;
using System;
using System.Collections.Generic;
using System.Text;

namespace RPGLibrary.Services
{
    public class ItemService
    {
        public IItemRepo itemrepo;

        public ItemService(IItemRepo itemrepo)
        {
            this.itemrepo = itemrepo;
        }

        public List<Item> DumpItems()
        {
            return itemrepo.DumpItems();
        }

        public void DeleteItem(int id)
        {
            itemrepo.DeleteItem(id);
        }

        public void AddItem(Item newitem)
        {
            itemrepo.AddItem(newitem);
        }

        public void FixURL()
        {
            itemrepo.FixURL();
        }
    }
}
