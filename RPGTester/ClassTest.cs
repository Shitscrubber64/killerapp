﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using RPGLibrary.Entities;
using RPGLibrary.Repositories;
using RPGLibrary.Services;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Text;

namespace RPGTester
{
    [TestClass()]
    public class ClassTest
    {
        public ClassTest()
        {
            // first let's clear any data from previous unit tests still stored in the database to prevent unexpected failures
            ClassService temp_service = new ClassService(new LocalSQLClassRepo());
            List<Class> temp_classes = new List<Class>();
            temp_classes = temp_service.DumpClasses();

            try
            {
                Class temp_class_CLONE = temp_classes.Find(x => x.name == "TEST_Clone");
                temp_service.DeleteClass(temp_class_CLONE.id); // delete TEST_Clone from database
            }
            catch (Exception) // pretty much guaranteed to be a ArgumentNullException and that's fine
            {
                Console.Write("Old data TEST_CLONE not found."); // probably won't even show in unit tests
            }

            try
            {
                Class temp_class_SABO = temp_classes.Find(x => x.name == "TEST_Saboteur");
                temp_service.DeleteClass(temp_class_SABO.id); // delete TEST_Saboteur from database
            }
            catch (Exception) // pretty much guaranteed to be a ArgumentNullException and that's fine
            {
                Console.Write("Old data TEST_SABOTEUR not found."); // probably won't even show in unit tests
            }
        }

        [TestMethod()]
        public void CanRetrieveClasses()
        {
            ClassService service = new ClassService(new LocalSQLClassRepo());
            List<Class> classes = new List<Class>();
            classes = service.DumpClasses();

            Assert.IsFalse(classes.Count == 0); // pass this test if there ARE instances in this list
        }

        [TestMethod()]
        [ExpectedException(typeof(SqlException), "The application failed to notice incorrect stats.")]
        public void CantInsertBadData()
        {
            ClassService service = new ClassService(new LocalSQLClassRepo());
            Class norris = new Class(0, "TEST_Norris", "The Norris tribe is known for its gamebreakingly high strength.", "norris", 99999.99999m, 10, 10, 10, 10);
            service.AddClass(norris); // trying to insert a new class with a decimal far beyond the accepted range
        }

        [TestMethod()]
        [ExpectedException(typeof(SqlException), "The application failed to notice this class already existed.")]
        public void CantInsertDuplicates()
        {
            ClassService service = new ClassService(new LocalSQLClassRepo());
            Class clone = new Class(0, "TEST_Clone", "This guy seems familiar...", "clone", 10m, 10, 10, 10, 10);
            service.AddClass(clone); // trying to insert a new class with (for now) unique data...

            service.AddClass(clone); // ...yet here we are doing it again
        }

        [TestMethod()]
        public void CantInject()
        {
            ClassService service = new ClassService(new LocalSQLClassRepo());
            Class saboteur = new Class(0, "TEST_Saboteur", "These tricky guys excel at bringing down an entire database.; DROP TABLE table_classes", "sabo", 10m, 10, 10, 10, 10);
            service.AddClass(saboteur); // trying to insert a new class with the intent of SQL injection

            List<Class> classes = new List<Class>();
            try
            {
                classes = service.DumpClasses();
                // well... duh, the test passes because you managed to retrieve data from this table after attempting to delete it via SQL injection
            }
            catch (Exception)
            {
                Assert.Fail();
            }
        }
    }
}
