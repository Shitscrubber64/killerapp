﻿using Microsoft.AspNetCore.Mvc;
using RPGLibrary.Entities;
using RPGLibrary.Repositories;
using RPGLibrary.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace KillerAppAurelia.Controllers
{
    [Route("api/[controller]/[action]")]
    public class ItemController : Controller
    {
        public ItemService service;

        
        public ItemController()
        {
            this.service = new ItemService(new LocalSQLItemRepo()); // this is pretty much the "main" part of code where you opt between different data sources - for now it's SQL
        }

        [HttpPost]
        public JsonResult dumpItems() // retrieve every row in class  table
        {
            return Json(service.DumpItems());
        }

        [HttpPost]
        public void deleteItem([FromBody] int id) // delete row in class table with matching ID
        {
            service.DeleteItem(id);
        }

        [HttpPost]
        public void addItem([FromBody] Item newitem) // add row to class table
        {
            service.AddItem(newitem);
        }

        [HttpPost]
        public void fixURL() // add row to class table
        {
            service.FixURL();
        }
  }
}
