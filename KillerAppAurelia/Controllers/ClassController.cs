﻿using Microsoft.AspNetCore.Mvc;
using RPGLibrary.Entities;
using RPGLibrary.Repositories;
using RPGLibrary.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace KillerAppAurelia.Controllers
{
    [Route("api/[controller]/[action]")]
    public class ClassController : Controller
    {
        public ClassService service;

        
        public ClassController()
        {
            this.service = new ClassService(new LocalSQLClassRepo()); // this is pretty much the "main" part of code where you opt between different data sources - for now it's SQL
        }

        [HttpPost]
        public JsonResult dumpClasses() // retrieve every row in class  table but also fill moveset
        {
            List<Class> data = new List<Class>();
            data = service.DumpClasses();
            List<Class> data_withmoves = new List<Class>();
            data_withmoves = service.FillWithMoves(data);
            return Json(data_withmoves);
        }

        [HttpPost]
        public void deleteClass([FromBody] int id) // delete row in class table with matching ID
        {
            service.DeleteClass(id);
        }

        [HttpPost]
        public void addClass([FromBody] Class newclass) // add row to class table
        {
            service.AddClass(newclass);
        }

        [HttpPost]
        public void addMove([FromBody] int idclass) // add row to class table
        {
          service.AddMove(idclass);
        }
  }
}
