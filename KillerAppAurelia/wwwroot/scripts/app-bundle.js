var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
define('app',["require", "exports", "aurelia-framework", "aurelia-fetch-client"], function (require, exports, aurelia_framework_1, aurelia_fetch_client_1) {
    "use strict";
    Object.defineProperty(exports, "__esModule", { value: true });
    let App = class App {
        constructor(http) {
            this.http = http;
            this.configHttp();
        }
        configureRouter(config, router) {
            config.title = 'Aurelia';
            config.map([
                { route: ['/', 'items'], name: 'items', moduleId: 'pages/editor/items' },
                { route: ['/', 'classes'], name: 'classes', moduleId: 'pages/editor/classes' },
                { route: ['/', 'newgame'], name: 'newgame', moduleId: 'pages/game/newgame' },
                { route: ['/', 'home'], name: 'home', moduleId: 'pages/home' },
            ]);
            this.router = router;
        }
        configHttp() {
            this.http.configure(config => {
                config
                    .withBaseUrl('api/')
                    .withDefaults({
                    method: "POST",
                    credentials: 'same-origin',
                    headers: {
                        'Accept': 'application/json',
                        'X-Requested-With': 'Fetch'
                    }
                })
                    .withInterceptor({
                    request(request) {
                        console.log(`Requesting ${request.method} ${request.url}`);
                        return request;
                    },
                    response(response) {
                        console.log(`Received ${response.status} ${response.url}`);
                        return response;
                    }
                });
            });
        }
    };
    App = __decorate([
        aurelia_framework_1.inject(aurelia_fetch_client_1.HttpClient),
        __metadata("design:paramtypes", [aurelia_fetch_client_1.HttpClient])
    ], App);
    exports.App = App;
});

//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFwcC50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7Ozs7SUFLQSxJQUFhLEdBQUcsR0FBaEI7UUFHSSxZQUFvQixJQUFnQjtZQUFoQixTQUFJLEdBQUosSUFBSSxDQUFZO1lBQ2hDLElBQUksQ0FBQyxVQUFVLEVBQUUsQ0FBQztRQUN0QixDQUFDO1FBR0QsZUFBZSxDQUFDLE1BQTJCLEVBQUUsTUFBYztZQUN2RCxNQUFNLENBQUMsS0FBSyxHQUFHLFNBQVMsQ0FBQztZQUN6QixNQUFNLENBQUMsR0FBRyxDQUFDO2dCQUNQLEVBQUUsS0FBSyxFQUFFLENBQUMsR0FBRyxFQUFFLE9BQU8sQ0FBQyxFQUFFLElBQUksRUFBRSxPQUFPLEVBQUUsUUFBUSxFQUFFLG9CQUFvQixFQUFFO2dCQUN4RSxFQUFFLEtBQUssRUFBRSxDQUFDLEdBQUcsRUFBRSxTQUFTLENBQUMsRUFBRSxJQUFJLEVBQUUsU0FBUyxFQUFFLFFBQVEsRUFBRSxzQkFBc0IsRUFBRTtnQkFFOUUsRUFBRSxLQUFLLEVBQUUsQ0FBQyxHQUFHLEVBQUUsU0FBUyxDQUFDLEVBQUUsSUFBSSxFQUFFLFNBQVMsRUFBRSxRQUFRLEVBQUUsb0JBQW9CLEVBQUU7Z0JBRTVFLEVBQUUsS0FBSyxFQUFFLENBQUMsR0FBRyxFQUFFLE1BQU0sQ0FBQyxFQUFFLElBQUksRUFBRSxNQUFNLEVBQUUsUUFBUSxFQUFFLFlBQVksRUFBRTthQUNqRSxDQUFDLENBQUM7WUFDSCxJQUFJLENBQUMsTUFBTSxHQUFHLE1BQU0sQ0FBQztRQUN6QixDQUFDO1FBRUQsVUFBVTtZQUNOLElBQUksQ0FBQyxJQUFJLENBQUMsU0FBUyxDQUFDLE1BQU07Z0JBQ3RCLE1BQU07cUJBQ0QsV0FBVyxDQUFDLE1BQU0sQ0FBQztxQkFDbkIsWUFBWSxDQUFDO29CQUNWLE1BQU0sRUFBRSxNQUFNO29CQUNkLFdBQVcsRUFBRSxhQUFhO29CQUMxQixPQUFPLEVBQUU7d0JBQ0wsUUFBUSxFQUFFLGtCQUFrQjt3QkFDNUIsa0JBQWtCLEVBQUUsT0FBTztxQkFDOUI7aUJBQ0osQ0FBQztxQkFDRCxlQUFlLENBQUM7b0JBQ2IsT0FBTyxDQUFDLE9BQU87d0JBQ1gsT0FBTyxDQUFDLEdBQUcsQ0FBQyxjQUFjLE9BQU8sQ0FBQyxNQUFNLElBQUksT0FBTyxDQUFDLEdBQUcsRUFBRSxDQUFDLENBQUM7d0JBQzNELE1BQU0sQ0FBQyxPQUFPLENBQUM7b0JBQ25CLENBQUM7b0JBQ0QsUUFBUSxDQUFDLFFBQWtCO3dCQUN2QixPQUFPLENBQUMsR0FBRyxDQUFDLFlBQVksUUFBUSxDQUFDLE1BQU0sSUFBSSxRQUFRLENBQUMsR0FBRyxFQUFFLENBQUMsQ0FBQzt3QkFDM0QsTUFBTSxDQUFDLFFBQVEsQ0FBQztvQkFDcEIsQ0FBQztpQkFDSixDQUFDLENBQUM7WUFDWCxDQUFDLENBQUMsQ0FBQztRQUNQLENBQUM7S0FDSixDQUFBO0lBN0NZLEdBQUc7UUFEZiwwQkFBTSxDQUFDLGlDQUFVLENBQUM7eUNBSVcsaUNBQVU7T0FIM0IsR0FBRyxDQTZDZjtJQTdDWSxrQkFBRyIsImZpbGUiOiJhcHAuanMiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBpbmplY3QgfSBmcm9tICdhdXJlbGlhLWZyYW1ld29yayc7XG5pbXBvcnQgeyBSb3V0ZXIsIFJvdXRlckNvbmZpZ3VyYXRpb24gfSBmcm9tICdhdXJlbGlhLXJvdXRlcic7XG5pbXBvcnQgeyBIdHRwQ2xpZW50IH0gZnJvbSAnYXVyZWxpYS1mZXRjaC1jbGllbnQnO1xyXG5cclxuQGluamVjdChIdHRwQ2xpZW50KVxuZXhwb3J0IGNsYXNzIEFwcCB7XHJcbiAgICByb3V0ZXI6IFJvdXRlcjtcblxyXG4gICAgY29uc3RydWN0b3IocHJpdmF0ZSBodHRwOiBIdHRwQ2xpZW50KSB7XG4gICAgICAgIHRoaXMuY29uZmlnSHR0cCgpO1xuICAgIH1cblxuXG4gICAgY29uZmlndXJlUm91dGVyKGNvbmZpZzogUm91dGVyQ29uZmlndXJhdGlvbiwgcm91dGVyOiBSb3V0ZXIpIHtcclxuICAgICAgICBjb25maWcudGl0bGUgPSAnQXVyZWxpYSc7XHJcbiAgICAgICAgY29uZmlnLm1hcChbXHJcbiAgICAgICAgICAgIHsgcm91dGU6IFsnLycsICdpdGVtcyddLCBuYW1lOiAnaXRlbXMnLCBtb2R1bGVJZDogJ3BhZ2VzL2VkaXRvci9pdGVtcycgfSxcbiAgICAgICAgICAgIHsgcm91dGU6IFsnLycsICdjbGFzc2VzJ10sIG5hbWU6ICdjbGFzc2VzJywgbW9kdWxlSWQ6ICdwYWdlcy9lZGl0b3IvY2xhc3NlcycgfSxcblxuICAgICAgICAgICAgeyByb3V0ZTogWycvJywgJ25ld2dhbWUnXSwgbmFtZTogJ25ld2dhbWUnLCBtb2R1bGVJZDogJ3BhZ2VzL2dhbWUvbmV3Z2FtZScgfSxcclxuXHJcbiAgICAgICAgICAgIHsgcm91dGU6IFsnLycsICdob21lJ10sIG5hbWU6ICdob21lJywgbW9kdWxlSWQ6ICdwYWdlcy9ob21lJyB9LFxyXG4gICAgICAgIF0pO1xuICAgICAgICB0aGlzLnJvdXRlciA9IHJvdXRlcjtcclxuICAgIH1cblxuICAgIGNvbmZpZ0h0dHAoKTogdm9pZCB7XG4gICAgICAgIHRoaXMuaHR0cC5jb25maWd1cmUoY29uZmlnID0+IHtcbiAgICAgICAgICAgIGNvbmZpZ1xuICAgICAgICAgICAgICAgIC53aXRoQmFzZVVybCgnYXBpLycpXG4gICAgICAgICAgICAgICAgLndpdGhEZWZhdWx0cyh7XG4gICAgICAgICAgICAgICAgICAgIG1ldGhvZDogXCJQT1NUXCIsXG4gICAgICAgICAgICAgICAgICAgIGNyZWRlbnRpYWxzOiAnc2FtZS1vcmlnaW4nLFxuICAgICAgICAgICAgICAgICAgICBoZWFkZXJzOiB7XG4gICAgICAgICAgICAgICAgICAgICAgICAnQWNjZXB0JzogJ2FwcGxpY2F0aW9uL2pzb24nLFxuICAgICAgICAgICAgICAgICAgICAgICAgJ1gtUmVxdWVzdGVkLVdpdGgnOiAnRmV0Y2gnXG4gICAgICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICB9KVxuICAgICAgICAgICAgICAgIC53aXRoSW50ZXJjZXB0b3Ioe1xuICAgICAgICAgICAgICAgICAgICByZXF1ZXN0KHJlcXVlc3QpIHtcbiAgICAgICAgICAgICAgICAgICAgICAgIGNvbnNvbGUubG9nKGBSZXF1ZXN0aW5nICR7cmVxdWVzdC5tZXRob2R9ICR7cmVxdWVzdC51cmx9YCk7XG4gICAgICAgICAgICAgICAgICAgICAgICByZXR1cm4gcmVxdWVzdDtcbiAgICAgICAgICAgICAgICAgICAgfSxcbiAgICAgICAgICAgICAgICAgICAgcmVzcG9uc2UocmVzcG9uc2U6IFJlc3BvbnNlKSB7XG4gICAgICAgICAgICAgICAgICAgICAgICBjb25zb2xlLmxvZyhgUmVjZWl2ZWQgJHtyZXNwb25zZS5zdGF0dXN9ICR7cmVzcG9uc2UudXJsfWApO1xuICAgICAgICAgICAgICAgICAgICAgICAgcmV0dXJuIHJlc3BvbnNlO1xuICAgICAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgfSk7XG4gICAgICAgIH0pO1xuICAgIH1cclxufSJdLCJzb3VyY2VSb290Ijoic3JjIn0=

define('environment',["require", "exports"], function (require, exports) {
    "use strict";
    Object.defineProperty(exports, "__esModule", { value: true });
    exports.default = {
        debug: true,
        testing: true
    };
});

//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImVudmlyb25tZW50LnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7OztJQUFBLGtCQUFlO1FBQ2IsS0FBSyxFQUFFLElBQUk7UUFDWCxPQUFPLEVBQUUsSUFBSTtLQUNkLENBQUMiLCJmaWxlIjoiZW52aXJvbm1lbnQuanMiLCJzb3VyY2VzQ29udGVudCI6WyJleHBvcnQgZGVmYXVsdCB7XG4gIGRlYnVnOiB0cnVlLFxuICB0ZXN0aW5nOiB0cnVlXG59O1xuIl0sInNvdXJjZVJvb3QiOiJzcmMifQ==

define('main',["require", "exports", "./environment"], function (require, exports, environment_1) {
    "use strict";
    Object.defineProperty(exports, "__esModule", { value: true });
    function configure(aurelia) {
        aurelia.use
            .standardConfiguration()
            .feature('resources');
        if (environment_1.default.debug) {
            aurelia.use.developmentLogging();
        }
        if (environment_1.default.testing) {
            aurelia.use.plugin('aurelia-testing');
        }
        aurelia.start().then(() => aurelia.setRoot());
    }
    exports.configure = configure;
});

//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIm1haW4udHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7O0lBR0EsbUJBQTBCLE9BQWdCO1FBQ3hDLE9BQU8sQ0FBQyxHQUFHO2FBQ1IscUJBQXFCLEVBQUU7YUFDdkIsT0FBTyxDQUFDLFdBQVcsQ0FBQyxDQUFDO1FBRXhCLEVBQUUsQ0FBQyxDQUFDLHFCQUFXLENBQUMsS0FBSyxDQUFDLENBQUMsQ0FBQztZQUN0QixPQUFPLENBQUMsR0FBRyxDQUFDLGtCQUFrQixFQUFFLENBQUM7UUFDbkMsQ0FBQztRQUVELEVBQUUsQ0FBQyxDQUFDLHFCQUFXLENBQUMsT0FBTyxDQUFDLENBQUMsQ0FBQztZQUN4QixPQUFPLENBQUMsR0FBRyxDQUFDLE1BQU0sQ0FBQyxpQkFBaUIsQ0FBQyxDQUFDO1FBQ3hDLENBQUM7UUFFRCxPQUFPLENBQUMsS0FBSyxFQUFFLENBQUMsSUFBSSxDQUFDLE1BQU0sT0FBTyxDQUFDLE9BQU8sRUFBRSxDQUFDLENBQUM7SUFDaEQsQ0FBQztJQWRELDhCQWNDIiwiZmlsZSI6Im1haW4uanMiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQge0F1cmVsaWF9IGZyb20gJ2F1cmVsaWEtZnJhbWV3b3JrJ1xuaW1wb3J0IGVudmlyb25tZW50IGZyb20gJy4vZW52aXJvbm1lbnQnO1xuXG5leHBvcnQgZnVuY3Rpb24gY29uZmlndXJlKGF1cmVsaWE6IEF1cmVsaWEpIHtcbiAgYXVyZWxpYS51c2VcbiAgICAuc3RhbmRhcmRDb25maWd1cmF0aW9uKClcbiAgICAuZmVhdHVyZSgncmVzb3VyY2VzJyk7XG5cbiAgaWYgKGVudmlyb25tZW50LmRlYnVnKSB7XG4gICAgYXVyZWxpYS51c2UuZGV2ZWxvcG1lbnRMb2dnaW5nKCk7XG4gIH1cblxuICBpZiAoZW52aXJvbm1lbnQudGVzdGluZykge1xuICAgIGF1cmVsaWEudXNlLnBsdWdpbignYXVyZWxpYS10ZXN0aW5nJyk7XG4gIH1cblxuICBhdXJlbGlhLnN0YXJ0KCkudGhlbigoKSA9PiBhdXJlbGlhLnNldFJvb3QoKSk7XG59XG4iXSwic291cmNlUm9vdCI6InNyYyJ9

var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
define('customelements/my-slider',["require", "exports", "aurelia-framework", "jquery", "bootstrap-slider"], function (require, exports, aurelia_framework_1, $) {
    "use strict";
    Object.defineProperty(exports, "__esModule", { value: true });
    let MySlider = class MySlider {
        constructor(element) {
            this.element = element;
        }
        attached() {
            let sel = $(this.element).children('input').first();
            this.slider = sel.slider();
            let y = this.slider.slider('setValue', this.value);
            this.slider.on("slideStop", (slideevt) => {
                this.value = this.slider.slider('getValue');
            });
        }
    };
    __decorate([
        aurelia_framework_1.bindable({ defaultBindingMode: aurelia_framework_1.bindingMode.twoWay }),
        __metadata("design:type", Number)
    ], MySlider.prototype, "value", void 0);
    MySlider = __decorate([
        aurelia_framework_1.autoinject,
        __metadata("design:paramtypes", [Element])
    ], MySlider);
    exports.MySlider = MySlider;
});

//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImN1c3RvbWVsZW1lbnRzL215LXNsaWRlci50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7Ozs7SUFNQSxJQUFhLFFBQVEsR0FBckI7UUFLSSxZQUFvQixPQUFnQjtZQUFoQixZQUFPLEdBQVAsT0FBTyxDQUFTO1FBRXBDLENBQUM7UUFFTSxRQUFRO1lBRVgsSUFBSSxHQUFHLEdBQVEsQ0FBQyxDQUFDLElBQUksQ0FBQyxPQUFPLENBQUMsQ0FBQyxRQUFRLENBQUMsT0FBTyxDQUFDLENBQUMsS0FBSyxFQUFFLENBQUM7WUFDekQsSUFBSSxDQUFDLE1BQU0sR0FBRyxHQUFHLENBQUMsTUFBTSxFQUFFLENBQUM7WUFDM0IsSUFBSSxDQUFDLEdBQUcsSUFBSSxDQUFDLE1BQU0sQ0FBQyxNQUFNLENBQUMsVUFBVSxFQUFFLElBQUksQ0FBQyxLQUFLLENBQUMsQ0FBQztZQUNuRCxJQUFJLENBQUMsTUFBTSxDQUFDLEVBQUUsQ0FBQyxXQUFXLEVBQUcsQ0FBQyxRQUFRO2dCQUNsQyxJQUFJLENBQUMsS0FBSyxHQUFHLElBQUksQ0FBQyxNQUFNLENBQUMsTUFBTSxDQUFDLFVBQVUsQ0FBQyxDQUFDO1lBQ2hELENBQUMsQ0FBQyxDQUFDO1FBQ1AsQ0FBQztLQUNKLENBQUE7SUFoQnlEO1FBQXJELDRCQUFRLENBQUMsRUFBRSxrQkFBa0IsRUFBRSwrQkFBVyxDQUFDLE1BQU0sRUFBRSxDQUFDOzsyQ0FBZTtJQUYzRCxRQUFRO1FBRHBCLDhCQUFVO3lDQU1zQixPQUFPO09BTDNCLFFBQVEsQ0FrQnBCO0lBbEJZLDRCQUFRIiwiZmlsZSI6ImN1c3RvbWVsZW1lbnRzL215LXNsaWRlci5qcyIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IGJpbmRhYmxlLCBiaW5kaW5nTW9kZSwgYXV0b2luamVjdCB9IGZyb20gJ2F1cmVsaWEtZnJhbWV3b3JrJztcbmltcG9ydCAqIGFzICQgZnJvbSAnanF1ZXJ5JztcbmltcG9ydCAnYm9vdHN0cmFwLXNsaWRlcic7XG5cblxyXG5AYXV0b2luamVjdFxyXG5leHBvcnQgY2xhc3MgTXlTbGlkZXIge1xuXG4gICAgQGJpbmRhYmxlKHsgZGVmYXVsdEJpbmRpbmdNb2RlOiBiaW5kaW5nTW9kZS50d29XYXkgfSkgdmFsdWUgOiBudW1iZXJcbiAgICBzbGlkZXI6IGFueTtcbiAgICBcbiAgICBjb25zdHJ1Y3Rvcihwcml2YXRlIGVsZW1lbnQ6IEVsZW1lbnQpIHtcblxyXG4gICAgfVxuXG4gICAgcHVibGljIGF0dGFjaGVkKClcbiAgICB7XHJcbiAgICAgICAgbGV0IHNlbDogYW55ID0gJCh0aGlzLmVsZW1lbnQpLmNoaWxkcmVuKCdpbnB1dCcpLmZpcnN0KCk7XHJcbiAgICAgICAgdGhpcy5zbGlkZXIgPSBzZWwuc2xpZGVyKCk7XHJcbiAgICAgICAgbGV0IHkgPSB0aGlzLnNsaWRlci5zbGlkZXIoJ3NldFZhbHVlJywgdGhpcy52YWx1ZSk7XHJcbiAgICAgICAgdGhpcy5zbGlkZXIub24oXCJzbGlkZVN0b3BcIiwgIChzbGlkZWV2dCkgPT4ge1xyXG4gICAgICAgICAgICB0aGlzLnZhbHVlID0gdGhpcy5zbGlkZXIuc2xpZGVyKCdnZXRWYWx1ZScpO1xyXG4gICAgICAgIH0pO1xyXG4gICAgfVxyXG59Il0sInNvdXJjZVJvb3QiOiJzcmMifQ==

define('pages/home',["require", "exports"], function (require, exports) {
    "use strict";
    Object.defineProperty(exports, "__esModule", { value: true });
    class home {
    }
    exports.home = home;
});

//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInBhZ2VzL2hvbWUudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7O0lBQUE7S0FFQztJQUZELG9CQUVDIiwiZmlsZSI6InBhZ2VzL2hvbWUuanMiLCJzb3VyY2VzQ29udGVudCI6WyJleHBvcnQgY2xhc3MgaG9tZSB7XG5cclxufSJdLCJzb3VyY2VSb290Ijoic3JjIn0=

define('resources/index',["require", "exports"], function (require, exports) {
    "use strict";
    Object.defineProperty(exports, "__esModule", { value: true });
    function configure(config) {
    }
    exports.configure = configure;
});

//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInJlc291cmNlcy9pbmRleC50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7SUFFQSxtQkFBMEIsTUFBOEI7SUFFeEQsQ0FBQztJQUZELDhCQUVDIiwiZmlsZSI6InJlc291cmNlcy9pbmRleC5qcyIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7RnJhbWV3b3JrQ29uZmlndXJhdGlvbn0gZnJvbSAnYXVyZWxpYS1mcmFtZXdvcmsnO1xuXG5leHBvcnQgZnVuY3Rpb24gY29uZmlndXJlKGNvbmZpZzogRnJhbWV3b3JrQ29uZmlndXJhdGlvbikge1xuICAvL2NvbmZpZy5nbG9iYWxSZXNvdXJjZXMoW10pO1xufVxuIl0sInNvdXJjZVJvb3QiOiJzcmMifQ==

var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
define('pages/editor/classes',["require", "exports", "aurelia-framework", "aurelia-fetch-client", "aurelia-event-aggregator", "aurelia-router"], function (require, exports, aurelia_framework_1, aurelia_fetch_client_1, aurelia_event_aggregator_1, aurelia_router_1) {
    "use strict";
    Object.defineProperty(exports, "__esModule", { value: true });
    class ClassItem {
        constructor() {
            this.name = "New class";
            this.desc = "Keep it short and specific!";
            this.base_atk = 10;
            this.base_def = 10;
            this.base_spatk = 10;
            this.base_spdef = 10;
            this.base_spd = 10;
        }
    }
    let classes = class classes {
        constructor(http, event, router) {
            this.http = http;
            this.event = event;
            this.router = router;
            this.test = 7;
            this.getClasses();
            this.newclass = new ClassItem();
        }
        getClasses() {
            this.http.fetch('class/dumpClasses')
                .then(response => response.json())
                .then(data => {
                this.classes = data;
                console.log(data);
            });
        }
        deleteClass(selclass) {
            this.http.fetch('class/deleteClass', {
                body: aurelia_fetch_client_1.json(selclass.id)
            });
            location.reload();
        }
        testing() {
            console.error(this.test);
        }
        addClass() {
            if (this.newclass.name == "" || this.newclass.desc == "") {
                alert("Every field is required!");
            }
            else {
                this.http.fetch('class/addClass', {
                    body: aurelia_fetch_client_1.json(this.newclass)
                });
                location.reload();
            }
        }
        addMove(idclass) {
            this.http.fetch('class/addMove', {
                body: aurelia_fetch_client_1.json(idclass)
            });
            location.reload();
        }
    };
    classes = __decorate([
        aurelia_framework_1.autoinject,
        __metadata("design:paramtypes", [aurelia_fetch_client_1.HttpClient, aurelia_event_aggregator_1.EventAggregator, aurelia_router_1.Router])
    ], classes);
    exports.classes = classes;
});

//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInBhZ2VzL2VkaXRvci9jbGFzc2VzLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7OztJQU1BO1FBQUE7WUFFSSxTQUFJLEdBQVcsV0FBVyxDQUFDO1lBQzNCLFNBQUksR0FBVyw2QkFBNkIsQ0FBQztZQUc3QyxhQUFRLEdBQVcsRUFBRSxDQUFDO1lBQ3RCLGFBQVEsR0FBVyxFQUFFLENBQUM7WUFDdEIsZUFBVSxHQUFXLEVBQUUsQ0FBQztZQUN4QixlQUFVLEdBQVcsRUFBRSxDQUFDO1lBQ3hCLGFBQVEsR0FBVyxFQUFFLENBQUM7UUFDMUIsQ0FBQztLQUFBO0lBR0QsSUFBYSxPQUFPLEdBQXBCO1FBS0ksWUFBb0IsSUFBZ0IsRUFBVSxLQUFzQixFQUFVLE1BQWM7WUFBeEUsU0FBSSxHQUFKLElBQUksQ0FBWTtZQUFVLFVBQUssR0FBTCxLQUFLLENBQWlCO1lBQVUsV0FBTSxHQUFOLE1BQU0sQ0FBUTtZQUY1RixTQUFJLEdBQVcsQ0FBQyxDQUFDO1lBR2IsSUFBSSxDQUFDLFVBQVUsRUFBRSxDQUFDO1lBQ2xCLElBQUksQ0FBQyxRQUFRLEdBQUcsSUFBSSxTQUFTLEVBQUUsQ0FBQztRQUNwQyxDQUFDO1FBRUQsVUFBVTtZQUVOLElBQUksQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLG1CQUFtQixDQUFDO2lCQUMvQixJQUFJLENBQUMsUUFBUSxJQUFJLFFBQVEsQ0FBQyxJQUFJLEVBQUUsQ0FBQztpQkFDakMsSUFBSSxDQUFDLElBQUk7Z0JBQ04sSUFBSSxDQUFDLE9BQU8sR0FBRyxJQUFJLENBQUM7Z0JBQ3BCLE9BQU8sQ0FBQyxHQUFHLENBQUMsSUFBSSxDQUFDLENBQUM7WUFDdEIsQ0FBQyxDQUFDLENBQUM7UUFDWCxDQUFDO1FBRUQsV0FBVyxDQUFDLFFBQVE7WUFDaEIsSUFBSSxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsbUJBQW1CLEVBQUU7Z0JBQ2pDLElBQUksRUFBRSwyQkFBSSxDQUFDLFFBQVEsQ0FBQyxFQUFFLENBQUM7YUFDMUIsQ0FBQyxDQUFDO1lBR0gsUUFBUSxDQUFDLE1BQU0sRUFBRSxDQUFDO1FBQ3RCLENBQUM7UUFFRCxPQUFPO1lBQ0gsT0FBTyxDQUFDLEtBQUssQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLENBQUM7UUFDN0IsQ0FBQztRQUdELFFBQVE7WUFDSixFQUFFLENBQUMsQ0FBQyxJQUFJLENBQUMsUUFBUSxDQUFDLElBQUksSUFBSSxFQUFFLElBQUksSUFBSSxDQUFDLFFBQVEsQ0FBQyxJQUFJLElBQUksRUFBRSxDQUFDLENBQ3pELENBQUM7Z0JBQ0csS0FBSyxDQUFDLDBCQUEwQixDQUFDLENBQUM7WUFDdEMsQ0FBQztZQUNELElBQUksQ0FDSixDQUFDO2dCQUNHLElBQUksQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLGdCQUFnQixFQUFFO29CQUM5QixJQUFJLEVBQUUsMkJBQUksQ0FBQyxJQUFJLENBQUMsUUFBUSxDQUFDO2lCQUM1QixDQUFDLENBQUM7Z0JBQ0gsUUFBUSxDQUFDLE1BQU0sRUFBRSxDQUFDO1lBQ3RCLENBQUM7UUFDTCxDQUFDO1FBRUQsT0FBTyxDQUFDLE9BQWU7WUFDbkIsSUFBSSxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsZUFBZSxFQUFFO2dCQUM3QixJQUFJLEVBQUUsMkJBQUksQ0FBQyxPQUFPLENBQUM7YUFDdEIsQ0FBQyxDQUFDO1lBQ0gsUUFBUSxDQUFDLE1BQU0sRUFBRSxDQUFDO1FBQ3RCLENBQUM7S0FDSixDQUFBO0lBdERZLE9BQU87UUFEbkIsOEJBQVU7eUNBTW1CLGlDQUFVLEVBQWlCLDBDQUFlLEVBQWtCLHVCQUFNO09BTG5GLE9BQU8sQ0FzRG5CO0lBdERZLDBCQUFPIiwiZmlsZSI6InBhZ2VzL2VkaXRvci9jbGFzc2VzLmpzIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgYXV0b2luamVjdCB9IGZyb20gJ2F1cmVsaWEtZnJhbWV3b3JrJztcbmltcG9ydCB7IEh0dHBDbGllbnQsIGpzb24gfSBmcm9tICdhdXJlbGlhLWZldGNoLWNsaWVudCc7XG5pbXBvcnQgeyBFdmVudEFnZ3JlZ2F0b3IgfSBmcm9tICdhdXJlbGlhLWV2ZW50LWFnZ3JlZ2F0b3InO1xuaW1wb3J0IHsgUm91dGVyIH0gZnJvbSAnYXVyZWxpYS1yb3V0ZXInO1xuaW1wb3J0ICogYXMgJCBmcm9tICdqcXVlcnknO1xuXG5jbGFzcyBDbGFzc0l0ZW0geyAvLyB2aXJ0dWFsbHkgaWRlbnRpY2FsIHRvIHRoZSBDIyB2ZXJzaW9uXG4gICAgaWQ6IG51bWJlcjtcbiAgICBuYW1lOiBzdHJpbmcgPSBcIk5ldyBjbGFzc1wiO1xuICAgIGRlc2M6IHN0cmluZyA9IFwiS2VlcCBpdCBzaG9ydCBhbmQgc3BlY2lmaWMhXCI7XG4gICAgc3ByaXRldXJsOiBzdHJpbmc7XG4gICAgbW92ZXNldDogc3RyaW5nW107XG4gICAgYmFzZV9hdGs6IG51bWJlciA9IDEwO1xuICAgIGJhc2VfZGVmOiBudW1iZXIgPSAxMDtcbiAgICBiYXNlX3NwYXRrOiBudW1iZXIgPSAxMDtcbiAgICBiYXNlX3NwZGVmOiBudW1iZXIgPSAxMDtcbiAgICBiYXNlX3NwZDogbnVtYmVyID0gMTA7XHJcbn1cblxuQGF1dG9pbmplY3RcbmV4cG9ydCBjbGFzcyBjbGFzc2VzIHtcbiAgICBjbGFzc2VzO1xuICAgIG5ld2NsYXNzOiBDbGFzc0l0ZW07XG4gICAgdGVzdDogbnVtYmVyID0gNztcblxuICAgIGNvbnN0cnVjdG9yKHByaXZhdGUgaHR0cDogSHR0cENsaWVudCwgcHJpdmF0ZSBldmVudDogRXZlbnRBZ2dyZWdhdG9yLCBwcml2YXRlIHJvdXRlcjogUm91dGVyKSB7XG4gICAgICAgIHRoaXMuZ2V0Q2xhc3NlcygpO1xuICAgICAgICB0aGlzLm5ld2NsYXNzID0gbmV3IENsYXNzSXRlbSgpO1xuICAgIH1cblxuICAgIGdldENsYXNzZXMoKSB7XG4gICAgICAgXHJcbiAgICAgICAgdGhpcy5odHRwLmZldGNoKCdjbGFzcy9kdW1wQ2xhc3NlcycpIC8vIG1ldGhvZCBkZWZpbmVkIGluIHRoZSBjb250cm9sbGVyIGZpbGVcclxuICAgICAgICAgICAgLnRoZW4ocmVzcG9uc2UgPT4gcmVzcG9uc2UuanNvbigpKVxyXG4gICAgICAgICAgICAudGhlbihkYXRhID0+IHtcclxuICAgICAgICAgICAgICAgIHRoaXMuY2xhc3NlcyA9IGRhdGE7XHJcbiAgICAgICAgICAgICAgICBjb25zb2xlLmxvZyhkYXRhKTsgLy8gZm9yIGRlYnVnIHB1cnBvc2VzXHJcbiAgICAgICAgICAgIH0pO1xyXG4gICAgfVxuXG4gICAgZGVsZXRlQ2xhc3Moc2VsY2xhc3MpIHtcclxuICAgICAgICB0aGlzLmh0dHAuZmV0Y2goJ2NsYXNzL2RlbGV0ZUNsYXNzJywge1xyXG4gICAgICAgICAgICBib2R5OiBqc29uKHNlbGNsYXNzLmlkKVxyXG4gICAgICAgIH0pO1xuICAgICAgICAvLyBhbGVydChcIkl0ZW0gaGFzIGJlZW4gZGVsZXRlZCFcIik7XG4gICAgICAgIC8vIHRoaXMuZ2V0Q2xhc3NlcygpO1xuICAgICAgICBsb2NhdGlvbi5yZWxvYWQoKTsgLy8gcmVmcmVzaCBlbnRpcmUgcGFnZTsgcmVsb2FkaW5nIG9ubHkgdGhlIGRhdGEgbGlzdCBpcyBub3QgcmVsaWFibGVcclxuICAgIH1cbiAgICBcbiAgICB0ZXN0aW5nKCkge1xuICAgICAgICBjb25zb2xlLmVycm9yKHRoaXMudGVzdCk7XHJcbiAgICB9XG5cblxuICAgIGFkZENsYXNzKCkge1xyXG4gICAgICAgIGlmICh0aGlzLm5ld2NsYXNzLm5hbWUgPT0gXCJcIiB8fCB0aGlzLm5ld2NsYXNzLmRlc2MgPT0gXCJcIilcbiAgICAgICAge1xuICAgICAgICAgICAgYWxlcnQoXCJFdmVyeSBmaWVsZCBpcyByZXF1aXJlZCFcIik7XHJcbiAgICAgICAgfVxuICAgICAgICBlbHNlXG4gICAgICAgIHtcbiAgICAgICAgICAgIHRoaXMuaHR0cC5mZXRjaCgnY2xhc3MvYWRkQ2xhc3MnLCB7XHJcbiAgICAgICAgICAgICAgICBib2R5OiBqc29uKHRoaXMubmV3Y2xhc3MpXHJcbiAgICAgICAgICAgIH0pO1xuICAgICAgICAgICAgbG9jYXRpb24ucmVsb2FkKCk7IC8vIHJlZnJlc2ggZW50aXJlIHBhZ2U7IHJlbG9hZGluZyBvbmx5IHRoZSBkYXRhIGxpc3QgaXMgbm90IHJlbGlhYmxlXHJcbiAgICAgICAgfVxyXG4gICAgfVxuXG4gICAgYWRkTW92ZShpZGNsYXNzOiBudW1iZXIpIHtcbiAgICAgICAgdGhpcy5odHRwLmZldGNoKCdjbGFzcy9hZGRNb3ZlJywge1xyXG4gICAgICAgICAgICBib2R5OiBqc29uKGlkY2xhc3MpXHJcbiAgICAgICAgfSk7XG4gICAgICAgIGxvY2F0aW9uLnJlbG9hZCgpO1xyXG4gICAgfVxyXG59Il0sInNvdXJjZVJvb3QiOiJzcmMifQ==

var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
define('pages/editor/items',["require", "exports", "aurelia-framework", "aurelia-fetch-client", "aurelia-event-aggregator", "aurelia-router"], function (require, exports, aurelia_framework_1, aurelia_fetch_client_1, aurelia_event_aggregator_1, aurelia_router_1) {
    "use strict";
    Object.defineProperty(exports, "__esModule", { value: true });
    class ItemItem {
        constructor() {
            this.name = "New item";
            this.desc = "Keep it short and specific!";
            this.mod_atk = 1.00;
            this.mod_def = 1.00;
            this.mod_spatk = 1.00;
            this.mod_spdef = 1.00;
            this.mod_spd = 1.00;
        }
    }
    let items = class items {
        constructor(http, event, router) {
            this.http = http;
            this.event = event;
            this.router = router;
            this.getItems();
            this.newitem = new ItemItem();
        }
        getItems() {
            this.http.fetch('item/dumpItems')
                .then(response => response.json())
                .then(data => {
                this.items = data;
                console.log(data);
            });
        }
        deleteItem(selitem) {
            this.http.fetch('item/deleteItem', {
                body: aurelia_fetch_client_1.json(selitem.id)
            });
            location.reload();
        }
        addItem() {
            if (this.newitem.name == "" || this.newitem.desc == "") {
                alert("Every field is required!");
            }
            else if (Number(this.newitem.mod_atk) > 0 && Number(this.newitem.mod_def) > 0 && Number(this.newitem.mod_spatk) > 0 && Number(this.newitem.mod_spdef) > 0 && Number(this.newitem.mod_spd) > 0) {
                this.http.fetch('item/addItem', {
                    body: aurelia_fetch_client_1.json(this.newitem)
                });
                location.reload();
            }
            else {
                alert("Stop deliberately trying to fuck up my database, you twat. Fill in a god damn number in the input elements.");
            }
        }
        fixURL() {
            this.http.fetch('item/fixURL');
            location.reload();
        }
    };
    items = __decorate([
        aurelia_framework_1.autoinject,
        __metadata("design:paramtypes", [aurelia_fetch_client_1.HttpClient, aurelia_event_aggregator_1.EventAggregator, aurelia_router_1.Router])
    ], items);
    exports.items = items;
});

//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInBhZ2VzL2VkaXRvci9pdGVtcy50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7Ozs7SUFLQTtRQUFBO1lBRUksU0FBSSxHQUFXLFVBQVUsQ0FBQztZQUMxQixTQUFJLEdBQVcsNkJBQTZCLENBQUM7WUFHN0MsWUFBTyxHQUFXLElBQUksQ0FBQztZQUN2QixZQUFPLEdBQVcsSUFBSSxDQUFDO1lBQ3ZCLGNBQVMsR0FBVyxJQUFJLENBQUM7WUFDekIsY0FBUyxHQUFXLElBQUksQ0FBQztZQUN6QixZQUFPLEdBQVcsSUFBSSxDQUFDO1FBQzNCLENBQUM7S0FBQTtJQUdELElBQWEsS0FBSyxHQUFsQjtRQUlJLFlBQW9CLElBQWdCLEVBQVUsS0FBc0IsRUFBVSxNQUFjO1lBQXhFLFNBQUksR0FBSixJQUFJLENBQVk7WUFBVSxVQUFLLEdBQUwsS0FBSyxDQUFpQjtZQUFVLFdBQU0sR0FBTixNQUFNLENBQVE7WUFDeEYsSUFBSSxDQUFDLFFBQVEsRUFBRSxDQUFDO1lBQ2hCLElBQUksQ0FBQyxPQUFPLEdBQUcsSUFBSSxRQUFRLEVBQUUsQ0FBQztRQUNsQyxDQUFDO1FBRUQsUUFBUTtZQUNKLElBQUksQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLGdCQUFnQixDQUFDO2lCQUM1QixJQUFJLENBQUMsUUFBUSxJQUFJLFFBQVEsQ0FBQyxJQUFJLEVBQUUsQ0FBQztpQkFDakMsSUFBSSxDQUFDLElBQUk7Z0JBQ04sSUFBSSxDQUFDLEtBQUssR0FBRyxJQUFJLENBQUM7Z0JBQ2xCLE9BQU8sQ0FBQyxHQUFHLENBQUMsSUFBSSxDQUFDLENBQUM7WUFDdEIsQ0FBQyxDQUFDLENBQUM7UUFDWCxDQUFDO1FBRUQsVUFBVSxDQUFDLE9BQU87WUFDZCxJQUFJLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxpQkFBaUIsRUFBRTtnQkFDL0IsSUFBSSxFQUFFLDJCQUFJLENBQUMsT0FBTyxDQUFDLEVBQUUsQ0FBQzthQUN6QixDQUFDLENBQUM7WUFDSCxRQUFRLENBQUMsTUFBTSxFQUFFLENBQUM7UUFDdEIsQ0FBQztRQUVELE9BQU87WUFDSCxFQUFFLENBQUMsQ0FBQyxJQUFJLENBQUMsT0FBTyxDQUFDLElBQUksSUFBSSxFQUFFLElBQUksSUFBSSxDQUFDLE9BQU8sQ0FBQyxJQUFJLElBQUksRUFBRSxDQUFDLENBQ3ZELENBQUM7Z0JBQ0csS0FBSyxDQUFDLDBCQUEwQixDQUFDLENBQUM7WUFDdEMsQ0FBQztZQUNELElBQUksQ0FBQyxFQUFFLENBQUMsQ0FBQyxNQUFNLENBQUMsSUFBSSxDQUFDLE9BQU8sQ0FBQyxPQUFPLENBQUMsR0FBRyxDQUFDLElBQUksTUFBTSxDQUFDLElBQUksQ0FBQyxPQUFPLENBQUMsT0FBTyxDQUFDLEdBQUcsQ0FBQyxJQUFJLE1BQU0sQ0FBQyxJQUFJLENBQUMsT0FBTyxDQUFDLFNBQVMsQ0FBQyxHQUFHLENBQUMsSUFBSSxNQUFNLENBQUMsSUFBSSxDQUFDLE9BQU8sQ0FBQyxTQUFTLENBQUMsR0FBRyxDQUFDLElBQUksTUFBTSxDQUFDLElBQUksQ0FBQyxPQUFPLENBQUMsT0FBTyxDQUFDLEdBQUcsQ0FBQyxDQUFDLENBQzlMLENBQUM7Z0JBQ0csSUFBSSxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsY0FBYyxFQUFFO29CQUM1QixJQUFJLEVBQUUsMkJBQUksQ0FBQyxJQUFJLENBQUMsT0FBTyxDQUFDO2lCQUMzQixDQUFDLENBQUM7Z0JBQ0gsUUFBUSxDQUFDLE1BQU0sRUFBRSxDQUFDO1lBQ3RCLENBQUM7WUFDRCxJQUFJLENBQ0osQ0FBQztnQkFDRyxLQUFLLENBQUMsNkdBQTZHLENBQUMsQ0FBQztZQUN6SCxDQUFDO1FBRUwsQ0FBQztRQUVELE1BQU07WUFDRixJQUFJLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxhQUFhLENBQUMsQ0FBQztZQUMvQixRQUFRLENBQUMsTUFBTSxFQUFFLENBQUM7UUFDdEIsQ0FBQztLQUNKLENBQUE7SUFoRFksS0FBSztRQURqQiw4QkFBVTt5Q0FLbUIsaUNBQVUsRUFBaUIsMENBQWUsRUFBa0IsdUJBQU07T0FKbkYsS0FBSyxDQWdEakI7SUFoRFksc0JBQUsiLCJmaWxlIjoicGFnZXMvZWRpdG9yL2l0ZW1zLmpzIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgYXV0b2luamVjdCB9IGZyb20gJ2F1cmVsaWEtZnJhbWV3b3JrJztcbmltcG9ydCB7IEh0dHBDbGllbnQsIGpzb24gfSBmcm9tICdhdXJlbGlhLWZldGNoLWNsaWVudCc7XG5pbXBvcnQgeyBFdmVudEFnZ3JlZ2F0b3IgfSBmcm9tICdhdXJlbGlhLWV2ZW50LWFnZ3JlZ2F0b3InO1xuaW1wb3J0IHsgUm91dGVyIH0gZnJvbSAnYXVyZWxpYS1yb3V0ZXInO1xuXG5jbGFzcyBJdGVtSXRlbSB7IC8vIHZpcnR1YWxseSBpZGVudGljYWwgdG8gdGhlIEMjIHZlcnNpb25cbiAgICBpZDogbnVtYmVyO1xuICAgIG5hbWU6IHN0cmluZyA9IFwiTmV3IGl0ZW1cIjtcbiAgICBkZXNjOiBzdHJpbmcgPSBcIktlZXAgaXQgc2hvcnQgYW5kIHNwZWNpZmljIVwiO1xuICAgIHNwcml0ZXVybDogc3RyaW5nO1xuICAgIHNsb3Q6IG51bWJlcjsgLy8gSlNPTiBzYWRseSBjb252ZXJ0cyBFTlVNIHRvIElOVCwgbm90IFNUUklORyAtIHRodXMgZm9yY2luZyBtZSB0byByZWx5IG9uIHNsb3QgaW50c1xuICAgIG1vZF9hdGs6IG51bWJlciA9IDEuMDA7XG4gICAgbW9kX2RlZjogbnVtYmVyID0gMS4wMDtcbiAgICBtb2Rfc3BhdGs6IG51bWJlciA9IDEuMDA7XG4gICAgbW9kX3NwZGVmOiBudW1iZXIgPSAxLjAwO1xuICAgIG1vZF9zcGQ6IG51bWJlciA9IDEuMDA7XHJcbn1cblxuQGF1dG9pbmplY3RcbmV4cG9ydCBjbGFzcyBpdGVtcyB7XG4gICAgaXRlbXM7XG4gICAgbmV3aXRlbTogSXRlbUl0ZW07XG5cbiAgICBjb25zdHJ1Y3Rvcihwcml2YXRlIGh0dHA6IEh0dHBDbGllbnQsIHByaXZhdGUgZXZlbnQ6IEV2ZW50QWdncmVnYXRvciwgcHJpdmF0ZSByb3V0ZXI6IFJvdXRlcikge1xuICAgICAgICB0aGlzLmdldEl0ZW1zKCk7XG4gICAgICAgIHRoaXMubmV3aXRlbSA9IG5ldyBJdGVtSXRlbSgpOyAvLyBuZWNlc3NhcnkgdG8gc2V0IGRlZmF1bHQgc3RhdHMgYW5kIGNoYW5nZSBpdHMgdHlwZSBmcm9tIHVuZGVmaW5lZCB0byBJdGVtSXRlbVxuICAgIH1cblxuICAgIGdldEl0ZW1zKCkge1xyXG4gICAgICAgIHRoaXMuaHR0cC5mZXRjaCgnaXRlbS9kdW1wSXRlbXMnKSAvLyBtZXRob2QgZGVmaW5lZCBpbiB0aGUgY29udHJvbGxlciBmaWxlXHJcbiAgICAgICAgICAgIC50aGVuKHJlc3BvbnNlID0+IHJlc3BvbnNlLmpzb24oKSlcclxuICAgICAgICAgICAgLnRoZW4oZGF0YSA9PiB7XHJcbiAgICAgICAgICAgICAgICB0aGlzLml0ZW1zID0gZGF0YTtcclxuICAgICAgICAgICAgICAgIGNvbnNvbGUubG9nKGRhdGEpOyAvLyBmb3IgZGVidWcgcHVycG9zZXNcclxuICAgICAgICAgICAgfSk7XHJcbiAgICB9XG5cbiAgICBkZWxldGVJdGVtKHNlbGl0ZW0pIHtcclxuICAgICAgICB0aGlzLmh0dHAuZmV0Y2goJ2l0ZW0vZGVsZXRlSXRlbScsIHtcclxuICAgICAgICAgICAgYm9keToganNvbihzZWxpdGVtLmlkKVxyXG4gICAgICAgIH0pO1xuICAgICAgICBsb2NhdGlvbi5yZWxvYWQoKTsgLy8gcmVmcmVzaCBlbnRpcmUgcGFnZTsgcmVsb2FkaW5nIG9ubHkgdGhlIGRhdGEgbGlzdCBpcyBub3QgcmVsaWFibGVcclxuICAgIH1cblxuICAgIGFkZEl0ZW0oKSB7XHJcbiAgICAgICAgaWYgKHRoaXMubmV3aXRlbS5uYW1lID09IFwiXCIgfHwgdGhpcy5uZXdpdGVtLmRlc2MgPT0gXCJcIilcbiAgICAgICAge1xuICAgICAgICAgICAgYWxlcnQoXCJFdmVyeSBmaWVsZCBpcyByZXF1aXJlZCFcIik7XHJcbiAgICAgICAgfVxuICAgICAgICBlbHNlIGlmIChOdW1iZXIodGhpcy5uZXdpdGVtLm1vZF9hdGspID4gMCAmJiBOdW1iZXIodGhpcy5uZXdpdGVtLm1vZF9kZWYpID4gMCAmJiBOdW1iZXIodGhpcy5uZXdpdGVtLm1vZF9zcGF0aykgPiAwICYmIE51bWJlcih0aGlzLm5ld2l0ZW0ubW9kX3NwZGVmKSA+IDAgJiYgTnVtYmVyKHRoaXMubmV3aXRlbS5tb2Rfc3BkKSA+IDApXG4gICAgICAgIHtcbiAgICAgICAgICAgIHRoaXMuaHR0cC5mZXRjaCgnaXRlbS9hZGRJdGVtJywge1xyXG4gICAgICAgICAgICAgICAgYm9keToganNvbih0aGlzLm5ld2l0ZW0pXHJcbiAgICAgICAgICAgIH0pO1xuICAgICAgICAgICAgbG9jYXRpb24ucmVsb2FkKCk7IC8vIHJlZnJlc2ggZW50aXJlIHBhZ2U7IHJlbG9hZGluZyBvbmx5IHRoZSBkYXRhIGxpc3QgaXMgbm90IHJlbGlhYmxlXHJcbiAgICAgICAgfVxuICAgICAgICBlbHNlXG4gICAgICAgIHtcbiAgICAgICAgICAgIGFsZXJ0KFwiU3RvcCBkZWxpYmVyYXRlbHkgdHJ5aW5nIHRvIGZ1Y2sgdXAgbXkgZGF0YWJhc2UsIHlvdSB0d2F0LiBGaWxsIGluIGEgZ29kIGRhbW4gbnVtYmVyIGluIHRoZSBpbnB1dCBlbGVtZW50cy5cIik7XHJcbiAgICAgICAgfVxuICAgICAgICBcclxuICAgIH1cblxuICAgIGZpeFVSTCgpIHtcbiAgICAgICAgdGhpcy5odHRwLmZldGNoKCdpdGVtL2ZpeFVSTCcpO1xuICAgICAgICBsb2NhdGlvbi5yZWxvYWQoKTtcclxuICAgIH1cclxufSJdLCJzb3VyY2VSb290Ijoic3JjIn0=

define('pages/game/newgame',["require", "exports"], function (require, exports) {
    "use strict";
    Object.defineProperty(exports, "__esModule", { value: true });
    class newgame {
    }
    exports.newgame = newgame;
});

//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInBhZ2VzL2dhbWUvbmV3Z2FtZS50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7SUFBQTtLQUVDO0lBRkQsMEJBRUMiLCJmaWxlIjoicGFnZXMvZ2FtZS9uZXdnYW1lLmpzIiwic291cmNlc0NvbnRlbnQiOlsiZXhwb3J0IGNsYXNzIG5ld2dhbWUge1xuXHJcbn0iXSwic291cmNlUm9vdCI6InNyYyJ9

define('text!app.html', ['module'], function(module) { module.exports = "<template bindable=\"router\">\n  <require from=\"./styles.css\"></require>\r\n  <div class=\"container\">\r\n    <div class=\"jumbotron\">\r\n      <h1>Onions and Wagons</h1>\r\n      <p>Fight your way through ten intense battles to finally reach your own freedom.</p>\r\n    </div>\r\n\r\n    <nav class=\"navbar navbar-default\">\r\n      <div class=\"container-fluid\">\r\n        <div class=\"navbar-header\">\r\n          <a class=\"navbar-brand\" href=\"#\">{Main Menu}</a>\r\n        </div>\r\n        <ul class=\"nav navbar-nav\">\r\n          <li><a route-href=\"route: home\">Home</a></li>\r\n          <li><a route-href=\"route: newgame\">New Game</a></li>\r\n          <li><a href=\"#\">Load Game</a></li>\r\n          <li><a route-href=\"route: classes\">Editor</a></li>\r\n        </ul>\r\n      </div>\r\n    </nav>\r\n\r\n    <div class=\"container\">\r\n      <router-view></router-view>\r\n    </div>\r\n\r\n  </div>\r\n\r\n</template>"; });
define('text!styles.css', ['module'], function(module) { module.exports = ".stat_positive {\n  color: limegreen; }\n\n.stat_negative {\n  color: red; }\n\ninput[type=range] {\n  -webkit-appearance: none;\n  width: 100%;\n  margin: 7.3px 0; }\n\ninput[type=range]:focus {\n  outline: none; }\n\ninput[type=range]::-webkit-slider-runnable-track {\n  width: 100%;\n  height: 11.4px;\n  cursor: pointer;\n  box-shadow: 1px 1px 1px #000000, 0px 0px 1px #0d0d0d;\n  background: rgba(48, 113, 169, 0.78);\n  border-radius: 1.3px;\n  border: 0.2px solid #010101; }\n\ninput[type=range]::-webkit-slider-thumb {\n  box-shadow: 0.9px 0.9px 1px #000031, 0px 0px 0.9px #00004b;\n  border: 1.8px solid #00001e;\n  height: 26px;\n  width: 26px;\n  border-radius: 15px;\n  background: #ffffff;\n  cursor: pointer;\n  -webkit-appearance: none;\n  margin-top: -7.5px; }\n\ninput[type=range]:focus::-webkit-slider-runnable-track {\n  background: rgba(54, 126, 189, 0.78); }\n\ninput[type=range]::-moz-range-track {\n  width: 100%;\n  height: 11.4px;\n  cursor: pointer;\n  box-shadow: 1px 1px 1px #000000, 0px 0px 1px #0d0d0d;\n  background: rgba(48, 113, 169, 0.78);\n  border-radius: 1.3px;\n  border: 0.2px solid #010101; }\n\ninput[type=range]::-moz-range-thumb {\n  box-shadow: 0.9px 0.9px 1px #000031, 0px 0px 0.9px #00004b;\n  border: 1.8px solid #00001e;\n  height: 26px;\n  width: 26px;\n  border-radius: 15px;\n  background: #ffffff;\n  cursor: pointer; }\n\ninput[type=range]::-ms-track {\n  width: 100%;\n  height: 11.4px;\n  cursor: pointer;\n  background: transparent;\n  border-color: transparent;\n  color: transparent; }\n\ninput[type=range]::-ms-fill-lower {\n  background: rgba(42, 100, 149, 0.78);\n  border: 0.2px solid #010101;\n  border-radius: 2.6px;\n  box-shadow: 1px 1px 1px #000000, 0px 0px 1px #0d0d0d; }\n\ninput[type=range]::-ms-fill-upper {\n  background: rgba(48, 113, 169, 0.78);\n  border: 0.2px solid #010101;\n  border-radius: 2.6px;\n  box-shadow: 1px 1px 1px #000000, 0px 0px 1px #0d0d0d; }\n\ninput[type=range]::-ms-thumb {\n  box-shadow: 0.9px 0.9px 1px #000031, 0px 0px 0.9px #00004b;\n  border: 1.8px solid #00001e;\n  height: 26px;\n  width: 26px;\n  border-radius: 15px;\n  background: #ffffff;\n  cursor: pointer;\n  height: 11.4px; }\n\ninput[type=range]:focus::-ms-fill-lower {\n  background: rgba(48, 113, 169, 0.78); }\n\ninput[type=range]:focus::-ms-fill-upper {\n  background: rgba(54, 126, 189, 0.78); }\n"; });
define('text!customelements/my-slider.html', ['module'], function(module) { module.exports = "<template> \n  <require from=\"bootstrap-slider/css/bootstrap-slider.min.css\"></require>\n  <input type=\"range\"  value.bind=\"value\"/>\n</template>"; });
define('text!pages/home.html', ['module'], function(module) { module.exports = "<template>\n  <p>Home page</p>\n</template>"; });
define('text!pages/editor/classes.html', ['module'], function(module) { module.exports = "<template>\n  <require from=\"../../customelements/my-slider\"></require>\n    <nav class=\"navbar navbar-default\">\r\n        <ul class=\"nav navbar-nav\">\r\n            <li><a route-href=\"route: classes\" class=\"active\">Classes</a></li>\r\n            <li><a route-href=\"route: items\">Items</a></li>\r\n        </ul>\r\n    </nav>\n    <!-- <my-slider value.bind=\"test\"></my-slider><button click.delegate=\"testing()\">test</button> -->\n    <button type=\"button\" class=\"btn btn-success\" data-toggle=\"modal\" data-target=\"#modal_newclass\"><span class=\"glyphicon glyphicon-plus\" aria-hidden=\"true\"></span> Click here to add a new class</button>\n    \n    <div class=\"modal fade\" id=\"modal_newclass\" tabindex=\"-1\" role=\"dialog\" aria-labelledby=\"myModalLabel\">\r\n        <div class=\"modal-dialog\" role=\"document\">\r\n            <div class=\"modal-content\">\r\n                <div class=\"modal-header\">\r\n                    <button type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-label=\"Close\"><span aria-hidden=\"true\">&times;</span></button>\r\n                    <h4 class=\"modal-title\" id=\"myModalLabel\">Add new class</h4>\r\n                </div>\r\n                <div class=\"modal-body\">\r\n                    <div class=\"row\">\n                        <div class=\"col-md-12\">\r\n                            <strong>Create your custom class below, then click the save button to apply changes.</strong>\r\n                        </div>\n                    </div>\n                    <div class=\"row\">\n                        <div class=\"col-md-3\">\n                            Name:\n                        </div>\n                        <div class=\"col-md-9\">\n                            <input type=\"text\" class=\"form-control\" id=\"input_name\" placeholder=\"New class\" value.bind=\"newclass.name\">\n                        </div>\n                    </div>\n                    <div class=\"row\">\r\n                        <div class=\"col-md-3\">\r\n                            Description:\r\n                        </div>\r\n                        <div class=\"col-md-9\">\r\n                            <input type=\"text\" class=\"form-control\" id=\"input_desc\" placeholder=\"Keep it short and specific!\" value.bind=\"newclass.desc\">\r\n                        </div>\r\n                    </div>\n                    <div class=\"row\">\r\n                        <div class=\"col-md-3\">\r\n                            Attack:\r\n                        </div>\r\n                        <div class=\"col-md-9\">\r\n                            <input type=\"range\" id=\"range_atk\" value=\"10\" min=\"1\" max=\"30\" value.bind=\"newclass.base_atk\">\r\n                        </div>\r\n                    </div>\n                    <div class=\"row\">\r\n                        <div class=\"col-md-3\">\r\n                            Defense:\r\n                        </div>\r\n                        <div class=\"col-md-9\">\r\n                            <input type=\"range\" id=\"range_def\" value=\"10\" min=\"1\" max=\"30\" value.bind=\"newclass.base_def\">\r\n                        </div>\r\n                    </div>\n                    <div class=\"row\">\r\n                        <div class=\"col-md-3\">\r\n                            Special Attack:\r\n                        </div>\r\n                        <div class=\"col-md-9\">\r\n                            <input type=\"range\" id=\"range_spatk\" value=\"10\" min=\"1\" max=\"30\" value.bind=\"newclass.base_spatk\">\r\n                        </div>\r\n                    </div>\n                    <div class=\"row\">\r\n                        <div class=\"col-md-3\">\r\n                            Special Defense:\r\n                        </div>\r\n                        <div class=\"col-md-9\">\r\n                            <input type=\"range\" id=\"range_spdef\" value=\"10\" min=\"1\" max=\"30\" value.bind=\"newclass.base_spdef\">\r\n                        </div>\r\n                    </div>\n                    <div class=\"row\">\r\n                        <div class=\"col-md-3\">\r\n                            Speed:\r\n                        </div>\r\n                      <div class=\"col-md-9\">\r\n                          <input type=\"range\" id=\"range_spd\" value=\"10\" min=\"1\" max=\"30\" value.bind=\"newclass.base_spd\">\r\n                      </div>\r\n                    </div>\r\n                </div>\r\n                <div class=\"modal-footer\">\r\n                    <button type=\"button\" class=\"btn btn-default\" data-dismiss=\"modal\"><span class=\"glyphicon glyphicon-remove\" aria-hidden=\"true\"></span> Close</button>\r\n                    <button type=\"button\" class=\"btn btn-primary\" data-dismiss=\"modal\" click.delegate=\"addClass()\"><span class=\"glyphicon glyphicon-ok\" aria-hidden=\"true\"></span> Save and exit</button>\r\n                </div>\r\n            </div>\r\n        </div>\r\n    </div>\n    \n    <h1>List of classes</h1>\n    <div class=\"panel-group\" id=\"accordion\">\r\n        <div repeat.for=\"class of classes\" class=\"panel panel-default\" id=\"panel_${class.id}\">\r\n            <div class=\"panel-heading\">\r\n                <h4 class=\"panel-title\">\r\n                  <span class=\"glyphicon glyphicon-triangle-bottom\" aria-hidden=\"true\"></span>\n                  <a data-toggle=\"collapse\" data-target=\"#collapse_${class.id}\" href=\"#collapse_${class.id}\" class=\"collapsed\"> ${class.name}</a>\r\n                </h4>\r\n\r\n            </div>\r\n            <div id=\"collapse_${class.id}\" class=\"panel-collapse collapse\">\r\n                <div class=\"panel-body\">\n                    <img src=\"img/sprites/${class.spriteurl}.png\" alt=\"Class icon\" class=\"img-responsive center-block\" />\n                    <h4>${class.name}</h4>\n                    <h5>${class.desc}</h5>\n                    \n                    <p>Stats:</p>\n                    <div class=\"progress\">\r\n                        <div class=\"progress-bar\" role=\"progressbar\" aria-valuenow=\"${class.base_atk * 10}\" aria-valuemin=\"0\" aria-valuemax=\"30\" style=\"width:${class.base_atk * 10}%\">\r\n                            ATK\r\n                        </div>\r\n                    </div>\n                    <div class=\"progress\">\r\n                        <div class=\"progress-bar\" role=\"progressbar\" aria-valuenow=\"${class.base_def * 10}\" aria-valuemin=\"0\" aria-valuemax=\"30\" style=\"width:${class.base_def * 10}%\">\r\n                            DEF\r\n                        </div>\r\n                    </div>\n                    <div class=\"progress\">\r\n                        <div class=\"progress-bar\" role=\"progressbar\" aria-valuenow=\"${class.base_spatk * 10}\" aria-valuemin=\"0\" aria-valuemax=\"30\" style=\"width:${class.base_spatk * 10}%\">\r\n                            SP.ATK\r\n                        </div>\r\n                    </div>\n                    <div class=\"progress\">\r\n                        <div class=\"progress-bar\" role=\"progressbar\" aria-valuenow=\"${class.base_spdef * 10}\" aria-valuemin=\"0\" aria-valuemax=\"30\" style=\"width:${class.base_spdef * 10}%\">\r\n                            SP.DEF\r\n                        </div>\r\n                    </div>\n                    <div class=\"progress\">\r\n                        <div class=\"progress-bar\" role=\"progressbar\" aria-valuenow=\"${class.base_spd * 10}\" aria-valuemin=\"0\" aria-valuemax=\"30\" style=\"width:${class.base_spd * 10}%\">\r\n                            SPD\r\n                        </div>\r\n                    </div>\n                    <br />\n                    <p>Moveset:</p>\n                    <ul class=\"list-group\" repeat.for=\"move of class.moveset\">\r\n                        <li class=\"list-group-item\">${move}</li>\r\n                    </ul>\n                    <button type=\"button\" class=\"btn btn-success\" click.delegate=\"addMove(class.id)\"><span class=\"glyphicon glyphicon-plus\" aria-hidden=\"true\"></span> Add random move</button>\n                    \n                    <p>---------------</p>\n                    <button type=\"button\" class=\"btn btn-danger\" click.delegate=\"deleteClass(class)\"><span class=\"glyphicon glyphicon-trash\" aria-hidden=\"true\"></span> Click here to delete this item</button>\n                </div>\r\n            </div>\r\n        </div> <!-- end of loop -->\n    </div>\r\n</template>"; });
define('text!pages/editor/items.html', ['module'], function(module) { module.exports = "<template>\n    <nav class=\"navbar navbar-default\">\r\n        <ul class=\"nav navbar-nav\">\r\n            <li><a route-href=\"route: classes\">Classes</a></li>\r\n            <li><a route-href=\"route: items\" class=\"active\">Items</a></li>\r\n        </ul>\r\n    </nav>\n    \n    <button type=\"button\" class=\"btn btn-success\" data-toggle=\"modal\" data-target=\"#modal_newitem\"><span class=\"glyphicon glyphicon-plus\" aria-hidden=\"true\"></span> Click here to add a new item</button>\n    <button type=\"button\" class=\"btn btn-warning\" click.delegate=\"fixURL()\"><span class=\"glyphicon glyphicon-wrench\" aria-hidden=\"true\"></span> Click here to fix potential undefined sprite URLs</button>  \n\n    <div class=\"modal fade\" id=\"modal_newitem\" tabindex=\"-1\" role=\"dialog\" aria-labelledby=\"myModalLabel\">\r\n        <div class=\"modal-dialog\" role=\"document\">\r\n            <div class=\"modal-content\">\r\n                <div class=\"modal-header\">\r\n                    <button type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-label=\"Close\"><span aria-hidden=\"true\">&times;</span></button>\r\n                    <h4 class=\"modal-title\" id=\"myModalLabel\">Add new item</h4>\r\n                </div>\r\n                <div class=\"modal-body\">\r\n                    <div class=\"row\">\n                        <div class=\"col-md-12\">\r\n                            <strong>Create your custom item below, then click the save button to apply changes.</strong>\r\n                        </div>\n                    </div>\n                    <div class=\"row\">\n                        <div class=\"col-md-3\">\n                            Name:\n                        </div>\n                        <div class=\"col-md-9\">\n                            <input type=\"text\" class=\"form-control\" id=\"input_name\" placeholder=\"New item\" value.bind=\"newitem.name\">\n                        </div>\n                    </div>\n                    <div class=\"row\">\r\n                        <div class=\"col-md-3\">\r\n                            Description:\r\n                        </div>\r\n                        <div class=\"col-md-9\">\r\n                            <input type=\"text\" class=\"form-control\" id=\"input_desc\" placeholder=\"Keep it short and specific!\" value.bind=\"newitem.desc\">\r\n                        </div>\r\n                    </div>\n                    <div class=\"row\">\r\n                      <div class=\"col-md-3\">\r\n                        Equipment slot:\r\n                      </div>\r\n                      <div class=\"col-md-9\">\r\n                        <select value.bind=\"newitem.slot\">\r\n                          <option value=\"0\" model.bind=\"0\">Weapon</option>\r\n                          <option value=\"1\" model.bind=\"1\">Head</option>\r\n                          <option value=\"2\" model.bind=\"2\">Chest</option>\r\n                          <option value=\"3\" model.bind=\"3\">Hands</option>\r\n                          <option value=\"4\" model.bind=\"4\">Feet</option>\r\n                        </select>\r\n                      </div>\r\n                    </div> <br /> <!-- could use some extra spacing here -->\n                    <div class=\"row\">\r\n                      <div class=\"col-md-3\">\r\n                        Attack modifier:\r\n                      </div>\r\n                      <div class=\"col-md-9\">\r\n                        <input type=\"number\" name=\"num_atk\" min=\"0.05\" max=\"10\" step=\"0.05\" value.bind=\"newitem.mod_atk\">\r\n                      </div>\r\n                    </div>\n                    <div class=\"row\">\r\n                      <div class=\"col-md-3\">\r\n                        Defense modifier:\r\n                      </div>\r\n                      <div class=\"col-md-9\">\r\n                        <input type=\"number\" name=\"num_def\" min=\"0.05\" max=\"10\" step=\"0.05\" value.bind=\"newitem.mod_def\">\r\n                      </div>\r\n                    </div>\n                    <div class=\"row\">\r\n                      <div class=\"col-md-3\">\r\n                        Sp. Attack modifier:\r\n                      </div>\r\n                      <div class=\"col-md-9\">\r\n                        <input type=\"number\" name=\"num_spatk\" min=\"0.05\" max=\"10\" step=\"0.05\" value.bind=\"newitem.mod_spatk\">\r\n                      </div>\r\n                    </div>\n                    <div class=\"row\">\r\n                      <div class=\"col-md-3\">\r\n                        Sp. Defense modifier:\r\n                      </div>\r\n                      <div class=\"col-md-9\">\r\n                        <input type=\"number\" name=\"num_spdef\" min=\"0.05\" max=\"10\" step=\"0.05\" value.bind=\"newitem.mod_spdef\">\r\n                      </div>\r\n                    </div>\n                    <div class=\"row\">\r\n                      <div class=\"col-md-3\">\r\n                        Speed modifier:\r\n                      </div>\r\n                      <div class=\"col-md-9\">\r\n                        <input type=\"number\" name=\"num_spd\" min=\"0.05\" max=\"10\" step=\"0.05\" value.bind=\"newitem.mod_spd\">\r\n                      </div>\r\n                    </div>\r\n                </div>\r\n                <div class=\"modal-footer\">\r\n                    <button type=\"button\" class=\"btn btn-default\" data-dismiss=\"modal\"><span class=\"glyphicon glyphicon-remove\" aria-hidden=\"true\"></span> Close</button>\r\n                    <button type=\"button\" class=\"btn btn-primary\" data-dismiss=\"modal\" click.delegate=\"addItem()\"><span class=\"glyphicon glyphicon-ok\" aria-hidden=\"true\"></span> Save and exit</button>\r\n                </div>\r\n            </div>\r\n        </div>\r\n    </div>\n    \n    <h1>List of items</h1>\n    <div class=\"row\">\r\n      <div class=\"panel-group\" id=\"accordion\">\r\n        <div repeat.for=\"item of items\" class=\"col-md-4\">\r\n          <div class=\"panel panel-default\" id=\"panel_${item.id}\">\n            <div class=\"panel-heading\">\r\n              <h4 class=\"panel-title\">\r\n                <span class=\"glyphicon glyphicon-triangle-bottom\" aria-hidden=\"true\"></span>\n                <a data-toggle=\"collapse\" data-target=\"#collapse_${item.id}\" href=\"#collapse_${item.id}\" class=\"collapsed\"> ${item.name}</a>\r\n              </h4>\r\n\r\n            </div>\r\n            <div id=\"collapse_${item.id}\" class=\"panel-collapse collapse\">\r\n              <div class=\"panel-body\">\r\n                <img src=\"img/sprites/${item.spriteurl}.png\" alt=\"Item icon\" class=\"img-responsive center-block\" />\r\n                <h4>${item.name}</h4>\r\n                <h5>${item.desc}</h5>\n                \n                Equipment slot: \n                <select value.bind=\"item.slot\" disabled>\r\n                  <option value=\"0\" model.bind=\"0\">Weapon</option>\r\n                  <option value=\"1\" model.bind=\"1\">Head</option>\r\n                  <option value=\"2\" model.bind=\"2\">Chest</option>\r\n                  <option value=\"3\" model.bind=\"3\">Hands</option>\n                  <option value=\"4\" model.bind=\"4\">Feet</option>\r\n                </select>\r\n\r\n                <p>Stat modifiers:</p>\r\n                <span class=\"${ item.mod_atk > 1 ? 'stat_positive' : '' } ${ item.mod_atk < 1 ? 'stat_negative' : '' }\">Attack: </span> <span class=\"badge\">${item.mod_atk}x</span> <br />\n                <span class=\"${ item.mod_def > 1 ? 'stat_positive' : '' } ${ item.mod_def < 1 ? 'stat_negative' : '' }\">Defense: </span> <span class=\"badge\">${item.mod_def}x</span> <br />\n                <span class=\"${ item.mod_spatk > 1 ? 'stat_positive' : '' } ${ item.mod_spatk < 1 ? 'stat_negative' : '' }\">Sp. Attack: </span> <span class=\"badge\">${item.mod_spatk}x</span> <br />\n                <span class=\"${ item.mod_spdef > 1 ? 'stat_positive' : '' } ${ item.mod_spdef < 1 ? 'stat_negative' : '' }\">Sp. Defense: </span> <span class=\"badge\">${item.mod_spdef}x</span> <br />\n                <span class=\"${ item.mod_spd > 1 ? 'stat_positive' : '' } ${ item.mod_spd < 1 ? 'stat_negative' : '' }\">Speed: </span> <span class=\"badge\">${item.mod_spd}x</span>\r\n                <p>---------------</p>\r\n                <button type=\"button\" class=\"btn btn-danger\" click.delegate=\"deleteItem(item)\"><span class=\"glyphicon glyphicon-trash\" aria-hidden=\"true\"></span> Click here to delete this item</button>\r\n              </div>\r\n            </div>\n           </div>\r\n        </div>\r\n      </div>\r\n    </div>\r\n</template>"; });
define('text!pages/game/newgame.html', ['module'], function(module) { module.exports = "<template>\n  <p>New game</p>\n</template>"; });
//# sourceMappingURL=app-bundle.js.map