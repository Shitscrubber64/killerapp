﻿import { autoinject } from 'aurelia-framework';
import { HttpClient, json } from 'aurelia-fetch-client';
import { EventAggregator } from 'aurelia-event-aggregator';
import { Router } from 'aurelia-router';
import * as $ from 'jquery';

class ClassItem { // virtually identical to the C# version
    id: number;
    name: string = "New class";
    desc: string = "Keep it short and specific!";
    spriteurl: string;
    moveset: string[];
    base_atk: number = 10;
    base_def: number = 10;
    base_spatk: number = 10;
    base_spdef: number = 10;
    base_spd: number = 10;
}

@autoinject
export class classes {
    classes;
    newclass: ClassItem;
    test: number = 7;

    constructor(private http: HttpClient, private event: EventAggregator, private router: Router) {
        this.getClasses();
        this.newclass = new ClassItem();
    }

    getClasses() {
       
        this.http.fetch('class/dumpClasses') // method defined in the controller file
            .then(response => response.json())
            .then(data => {
                this.classes = data;
                console.log(data); // for debug purposes
            });
    }

    deleteClass(selclass) {
        this.http.fetch('class/deleteClass', {
            body: json(selclass.id)
        });
        // alert("Item has been deleted!");
        // this.getClasses();
        location.reload(); // refresh entire page; reloading only the data list is not reliable
    }
    
    testing() {
        console.error(this.test);
    }


    addClass() {
        if (this.newclass.name == "" || this.newclass.desc == "")
        {
            alert("Every field is required!");
        }
        else
        {
            this.http.fetch('class/addClass', {
                body: json(this.newclass)
            });
            location.reload(); // refresh entire page; reloading only the data list is not reliable
        }
    }

    addMove(idclass: number) {
        this.http.fetch('class/addMove', {
            body: json(idclass)
        });
        location.reload();
    }
}