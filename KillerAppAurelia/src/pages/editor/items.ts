﻿import { autoinject } from 'aurelia-framework';
import { HttpClient, json } from 'aurelia-fetch-client';
import { EventAggregator } from 'aurelia-event-aggregator';
import { Router } from 'aurelia-router';

class ItemItem { // virtually identical to the C# version
    id: number;
    name: string = "New item";
    desc: string = "Keep it short and specific!";
    spriteurl: string;
    slot: number; // JSON sadly converts ENUM to INT, not STRING - thus forcing me to rely on slot ints
    mod_atk: number = 1.00;
    mod_def: number = 1.00;
    mod_spatk: number = 1.00;
    mod_spdef: number = 1.00;
    mod_spd: number = 1.00;
}

@autoinject
export class items {
    items;
    newitem: ItemItem;

    constructor(private http: HttpClient, private event: EventAggregator, private router: Router) {
        this.getItems();
        this.newitem = new ItemItem(); // necessary to set default stats and change its type from undefined to ItemItem
    }

    getItems() {
        this.http.fetch('item/dumpItems') // method defined in the controller file
            .then(response => response.json())
            .then(data => {
                this.items = data;
                console.log(data); // for debug purposes
            });
    }

    deleteItem(selitem) {
        this.http.fetch('item/deleteItem', {
            body: json(selitem.id)
        });
        location.reload(); // refresh entire page; reloading only the data list is not reliable
    }

    addItem() {
        if (this.newitem.name == "" || this.newitem.desc == "")
        {
            alert("Every field is required!");
        }
        else if (Number(this.newitem.mod_atk) > 0 && Number(this.newitem.mod_def) > 0 && Number(this.newitem.mod_spatk) > 0 && Number(this.newitem.mod_spdef) > 0 && Number(this.newitem.mod_spd) > 0)
        {
            this.http.fetch('item/addItem', {
                body: json(this.newitem)
            });
            location.reload(); // refresh entire page; reloading only the data list is not reliable
        }
        else
        {
            alert("Stop deliberately trying to fuck up my database, you twat. Fill in a god damn number in the input elements.");
        }
        
    }

    fixURL() {
        this.http.fetch('item/fixURL');
        location.reload();
    }
}