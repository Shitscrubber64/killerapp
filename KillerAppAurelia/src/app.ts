import { inject } from 'aurelia-framework';
import { Router, RouterConfiguration } from 'aurelia-router';
import { HttpClient } from 'aurelia-fetch-client';

@inject(HttpClient)
export class App {
    router: Router;

    constructor(private http: HttpClient) {
        this.configHttp();
    }


    configureRouter(config: RouterConfiguration, router: Router) {
        config.title = 'Aurelia';
        config.map([
            { route: ['/', 'items'], name: 'items', moduleId: 'pages/editor/items' },
            { route: ['/', 'classes'], name: 'classes', moduleId: 'pages/editor/classes' },

            { route: ['/', 'newgame'], name: 'newgame', moduleId: 'pages/game/newgame' },

            { route: ['/', 'home'], name: 'home', moduleId: 'pages/home' },
        ]);
        this.router = router;
    }

    configHttp(): void {
        this.http.configure(config => {
            config
                .withBaseUrl('api/')
                .withDefaults({
                    method: "POST",
                    credentials: 'same-origin',
                    headers: {
                        'Accept': 'application/json',
                        'X-Requested-With': 'Fetch'
                    }
                })
                .withInterceptor({
                    request(request) {
                        console.log(`Requesting ${request.method} ${request.url}`);
                        return request;
                    },
                    response(response: Response) {
                        console.log(`Received ${response.status} ${response.url}`);
                        return response;
                    }
                });
        });
    }
}