﻿import { bindable, bindingMode, autoinject } from 'aurelia-framework';
import * as $ from 'jquery';
import 'bootstrap-slider';


@autoinject
export class MySlider {

    @bindable({ defaultBindingMode: bindingMode.twoWay }) value : number
    slider: any;
    
    constructor(private element: Element) {

    }

    public attached()
    {
        let sel: any = $(this.element).children('input').first();
        this.slider = sel.slider();
        let y = this.slider.slider('setValue', this.value);
        this.slider.on("slideStop",  (slideevt) => {
            this.value = this.slider.slider('getValue');
        });
    }
}